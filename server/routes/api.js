var destiny_api = require('../destiny_api'),
    express = require('express'),
    url = require('url'),
    router = express.Router(),
    api = {};

for(var route in destiny_api) {
    api[route] = {};
    generateEndpoints(route, destiny_api[route]);
}

router.get('/reflect', function(req, res) {
    res.json(api);
});

function generateEndpoints(route, endpoints) {
    for(var endpoint in endpoints) {
        if(!endpoints.hasOwnProperty(endpoint)) {
            continue;
        }
        var path = url.resolve('/' + route + '/', endpoint),
            request = endpoints[endpoint];

        addEndpoint(path, request);
        api[route][endpoint] = {
            method: request.method,
            required: request.required
        }
    }
}

function addEndpoint(path, request) {
    router.get(path, function(req, res) {
        request(req.query)
            .then(function(response) {
                res.json(response);
            })
            .catch(function(error) {
                console.log(error);
            });
    });
}

module.exports = router;