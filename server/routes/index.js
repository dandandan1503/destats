function routingIndex(app) {
    app.use('/character' , require('./character'));
    app.use('/group' , require('./group'));
    app.use('/api', require('./api'));
}

module.exports = routingIndex;