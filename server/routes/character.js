var express = require('express'),
    router = express.Router(),
    api = require('../destiny_api');
    constants = require('../utilities').constants;

router.get('/', function(req, res) {
    api.search.searchDestinyPlayer({
        membershipType: constants.Platform.Live,
        displayName: 'dandandan1503'
    }).then(function(response) {
        api.account.summary({
            membershipType: constants.Platform.Live,
            destinyMembershipId: response[0].membershipId
        }).then(function(response) {
            res.json(response);
        });
    });
});

module.exports = router;

