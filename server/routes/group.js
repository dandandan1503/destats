var express = require('express'),
    router = express.Router(),
    api = require('../destiny_api');
    constants = require('../utilities').constants;

router.get('/', function(req, res) {
    api.group.getMembersV3({
        groupId: 627267,
        currentPage: 2
    }).then(function(response) {
        res.json(response);
    });
});

module.exports = router;

