var express = require('express'),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
    cors = require('express-cors'),
    app = express(),
    port = process.env.PORT || 8081;

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(cors({
    allowedOrigins: [
        'localhost:*',
        'dmilsolutions.com',
        '*.dmilsolutions.com'
    ]
}));

require('./routes')(app);

app.listen(port);

console.log('listening on port: ' + port);