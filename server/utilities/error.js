module.exports = function(message) {
    throw new DestinyAPIError(message);
};

function DestinyAPIError(message) {
    Error.call(this);
    Error.captureStackTrace(this, this.constructor);

    this.name = this.constructor.name; // function name as error name
    this.message = message;
}