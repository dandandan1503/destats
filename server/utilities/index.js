module.exports = {
    constants: require('./constants'),
    enumeration: require('./enumeration'),
    error: require('./error')
};