module.exports = function enumeration() {
    var content = Object.create({
        add: function(key, value) {
            this[this[key] = value] = key;
            return this;
        }
    });
    return content;
};