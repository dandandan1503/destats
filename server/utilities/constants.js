var enumeration = require('./enumeration');
module.exports = {
    Platform: enumeration()
        .add('Live', 1)
        .add('PS4', 2),
    Parameters: {
        activityId: 'activityId',
        characterId: 'characterId',
        destinyMembershipId: 'destinyMembershipId',
        displayName: 'displayName',
        groupId: 'groupId',
        membershipId: 'membershipId',
        membershipType: 'membershipType',
        name: 'name'
    },
    Method: {
        GET: 'GET',
        POST: 'POST'
    },
    ActivityMode: enumeration()
        .add('None', 0)
        .add('Story', 2)
        .add('Strike', 3)
        .add('Raid', 4)
        .add('AllPvP', 5)
        .add('Patrol', 6)
        .add('AllPvE', 7)
        .add('PvPIntroduction', 8)
        .add('ThreeVsThree', 9)
        .add('Control', 10)
        .add('Lockdown', 11)
        .add('Team', 12)
        .add('FreeForAll', 13)
        .add('Nightfall', 16)
        .add('Heroic', 17)
};