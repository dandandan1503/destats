var api = {
    account: require('./account'),
    group: require('./group'),
    search: require('./search'),
    stats: require('./stats')
};

module.exports = api;