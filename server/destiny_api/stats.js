var buildRequest = require('./apiUtilities/index').buildRequest,
    constants = require('./../utilities/index').constants;
module.exports = {
    account: buildRequest({
        path: 'Stats/Account/{membershipType}/{destinyMembershipId}',
        required: [
            constants.Parameters.membershipType,
            constants.Parameters.destinyMembershipId
        ]
    }),
    activityHistory: buildRequest({
        path: 'Stats/ActivityHistory/{membershipType}/{destinyMembershipId}/{characterId}',
        required: [
            constants.Parameters.membershipType,
            constants.Parameters.destinyMembershipId,
            constants.Parameters.characterId
        ]
    }),
    aggregateActivityStats: buildRequest({
        path: 'Stats/AggregateActivityStats/{membershipType}/{destinyMembershipId}/{characterId}',
        required: [
            constants.Parameters.membershipType,
            constants.Parameters.destinyMembershipId,
            constants.Parameters.characterId
        ]
    }),
    all: buildRequest({
        path: 'Stats/{membershipType}/{destinyMembershipId}/{characterId}',
        required: [
            constants.Parameters.membershipType,
            constants.Parameters.destinyMembershipId,
            constants.Parameters.characterId
        ]
    }),
    definition: buildRequest({
        path: 'Stats/Definition',
        required: []
    }),
    postGameCarnageReport: buildRequest({
        path: 'Stats/PostGameCarnageReport/{activityId}',
        required: [
            constants.Parameters.activityId
        ]
    }),
    uniqueWeapons: buildRequest({
        path: 'Stats/UniqueWeapons/{membershipType}/{destinyMembershipId}/{characterId}',
        required: [
            constants.Parameters.membershipType,
            constants.Parameters.destinyMembershipId,
            constants.Parameters.characterId
        ]
    })
};