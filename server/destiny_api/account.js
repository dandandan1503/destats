var buildRequest = require('./apiUtilities').buildRequest,
    constants = require('./../utilities').constants;
module.exports = {
    summary: buildRequest({
        path: '{membershipType}/Account/{destinyMembershipId}/Summary',
        required: [
            constants.Parameters.membershipType,
            constants.Parameters.destinyMembershipId
        ]
    }),
    items: buildRequest({
        path: '{membershipType}/Account/{destinyMembershipId}/Items',
        required: [
            constants.Parameters.membershipType,
            constants.Parameters.destinyMembershipId
        ]
    }),
    character: buildRequest({
        path: '{membershipType}/Account/{destinyMembershipId}/Character/{characterId}',
        required: [
            constants.Parameters.membershipType,
            constants.Parameters.destinyMembershipId,
            constants.Parameters.characterId
        ]
    }),
    characterActivities: buildRequest({
        path: '{membershipType}/Account/{destinyMembershipId}/Character/{characterId}/Activities',
        required: [
            constants.Parameters.membershipType,
            constants.Parameters.destinyMembershipId,
            constants.Parameters.characterId
        ]
    }),
    // Use summary instead
    characterInventory: buildRequest({
        path: '{membershipType}/Account/{destinyMembershipId}/Character/{characterId}/Inventory',
        required: [
            constants.Parameters.membershipType,
            constants.Parameters.destinyMembershipId,
            constants.Parameters.characterId
        ]
    }),
    characterInventorySummary: buildRequest({
        path: '{membershipType}/Account/{destinyMembershipId}/Character/{characterId}/Inventory/Summary',
        required: [
            constants.Parameters.membershipType,
            constants.Parameters.destinyMembershipId,
            constants.Parameters.characterId
        ]
    }),
    characterProgression: buildRequest({
        path: '{membershipType}/Account/{destinyMembershipId}/Character/{characterId}/Progression',
        required: [
            constants.Parameters.membershipType,
            constants.Parameters.destinyMembershipId,
            constants.Parameters.characterId
        ]
    })
};