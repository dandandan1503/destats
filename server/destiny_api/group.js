var buildRequest = require('./apiUtilities/index').buildRequest,
    constants = require('./../utilities/index').constants;
module.exports = {
    getMembersV3: buildRequest({
        path: 'Group/{groupId}/MembersV3',
        required: [
            constants.Parameters.groupId
        ],
        platform: false
    })
};