var config = require('./../../config'),
    promise = require('es6-promise').Promise,
    url = require('url'),
    utilities = require('../../utilities/index'),
    _ = require('lodash');

    require('isomorphic-fetch');

module.exports = function buildRequest(requestTemplate) {
    var headers = {
            'X-API-Key': config.apiKey,
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        requestTemplate = _.extend({
            platform: true,
            method: utilities.constants.Method.GET
        }, requestTemplate || {}),
        baseUrl = config.host;

    if(requestTemplate.platform) {
        baseUrl = url.resolve(baseUrl, config.platform);
    }

    // Cheesy hack because wtf node
    if(baseUrl[baseUrl.length - 1] !== '/') {
        baseUrl += '/';
    }

    var req = function (params) {
        requestTemplate.required.forEach(function (paramName) {
            if (!params.hasOwnProperty(paramName)) {
                utilities.error('Missing param `' + paramName + '` in request: \n' + requestTemplate.path);
            }
        });

        var path = requestTemplate.path,
            matches = path.match(/\{(.*?)\}/g),
            body = {},
            payload = {
                headers: headers,
                method: requestTemplate.method
            };

        for(var param in params) {
            if(requestTemplate.required.indexOf(param) !== -1) {
               continue;
            }
            body[param] = params[param];
        }

        // Assign url path parts
        matches.forEach(function (match) {
            var key = match.substring(1, match.length - 1);
            if (params[key]) {
                path = path.replace(match, params[key]);
            }
        });

        switch(requestTemplate.method) {
            // Build and append query string
            case utilities.constants.Method.GET:
                var qs = [];
                for(var param in body) {
                    qs.push([param, body[param]].join('='));
                }
                path = [path, qs].join('?');
                break;
            // Stringify and attach request body
            case utilities.constants.Method.POST:
                payload.body = JSON.stringify(body);
                break;
        }

        console.log(url.resolve(baseUrl, path));

        return promise.resolve().then(function () {
                return fetch(url.resolve(baseUrl, path), payload);
            })
            .then(json)
            .then(unwrap)
            .catch(function (err) {
                console.log(err);
                return err;
            });
    };
    req.method = requestTemplate.method;
    req.required = requestTemplate.required;
    return req;
};

function json(response) {
    return response.json();
}

function unwrap(response) {
    if (response.Response && response.Response.data) {
        return response.Response.data;
    } else if (response.Response) {
        return response.Response;
    } else {
        return response;
    }
}