var buildRequest = require('./apiUtilities/index').buildRequest,
    constants = require('./../utilities/index').constants;
module.exports = {
    searchDestinyPlayer: buildRequest({
        path: 'SearchDestinyPlayer/{membershipType}/{displayName}',
        required: [
            constants.Parameters.membershipType,
            constants.Parameters.displayName
        ]
    })
};