(function() {
  'use strict';

  angular.module('destats', [
    'ngAnimate',
    'ngCookies',
    'ngTouch',
    'ngSanitize',
    'ngMessages',
    'ngAria',
    'ngRoute',
    'ngMaterial',
    'toastr'
  ]);

})();
