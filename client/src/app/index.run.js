(function() {
  'use strict';

  angular
    .module('destats')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log, dataService) {

    $log.debug('runBlock end');
  }

})();
