(function() {
  'use strict';

  angular
    .module('destats')
    .config(routeConfig);

  function routeConfig($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .when('/player', {
        templateUrl: 'app/player/player.html',
        controller: 'PlayerController',
        controllerAs: 'player'
      })
      .when('/clan', {
        templateUrl: 'app/clan/clan.html',
        controller: 'ClanController',
        controllerAs: 'clan'
      })
      .otherwise({
        redirectTo: '/'
      });
  }

})();
