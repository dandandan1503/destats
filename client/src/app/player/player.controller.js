(function () {
  angular.module('destats')
    .controller('PlayerController', PlayerController);

  PlayerController.$inject = ['$scope', 'dataService', 'viewModel'];

  function PlayerController($scope, dataService, viewModel) {
    ////
    // Setup
    ////
    var ctrl = this;

    ////
    // API
    ////
    // Values
    ctrl.gamerTag = 'dandandan1503';
    ctrl.viewModel = viewModel;
    ctrl.searched = false;

    // Methods
    ctrl.searchPlayer = searchPlayer;

    ////
    // Scope
    ////
    $scope.bungie = viewModel.bungie;

    ////
    // Functions
    ////


    function searchPlayer() {
      if (viewModel.currentPlayer) {
        delete viewModel.currentPlayer;
      }
      dataService.search.searchDestinyPlayer({
          membershipType: 1,
          displayName: ctrl.gamerTag
        })
        .then(function (data) {
          ctrl.searched = true;
          viewModel.currentPlayer = data[0];
          dataService.account.summary({
              membershipType: 1,
              destinyMembershipId: viewModel.currentPlayer.membershipId,
            })
            .then(function (data) {
              angular.extend(viewModel.currentPlayer, data);
            })
        })
    }


  }
})();
