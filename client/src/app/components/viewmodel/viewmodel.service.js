(function () {
  angular.module('destats')
    .factory('viewModel', ViewModelService);

  ViewModelService.$inject = [];

  function ViewModelService() {
    ////
    // Fields
    ////
    var model = {
      bungie: {
        url: 'http://bungie.net'
      }
    };

    ////
    // API
    ////
    return model;

    ////
    // Functions
    ////
  }
})();
