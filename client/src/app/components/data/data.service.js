(function () {
  angular.module('destats')
    .provider('dataService', DataService);

  ////
  // Provider Fields
  ////
  var _host = 'loaclhost',
    _port = 80,
    _protocol = 'http';


  function baseUrl() {
    return _protocol + '://' + _host + (_port ? ':' + _port : '');
  }

  function DataService() {
    ////
    // Provider
    ////
    this.$get = function ($injector) {
      return $injector.invoke(dataService);
    };
    this.$get.$inject = ['$injector'];

    this.setHost = function (host) {
      _host = host;
    };
    this.setPort = function (port) {
      _port = port;
    };
    this.setProtocol = function (protocol) {
      _protocol = protocol;
    }
  }


  dataService.$inject = ['$http', '$log'];
  function dataService($http, $log) {
    ////
    // Fields
    ////
    var basePath = baseUrl() + '/api',
      _service = {};
    $http.get(basePath + '/reflect')
      .then(function (response) {
        $log.info(response.data);
        if (!response.data) {
          return;
        }
        for (var service in response.data) {
          _service[service] = {};
          for (var config in response.data[service]) {
            _service[service][config] = request([service, config].join('/'), response.data[service][config]);
          }
        }
        $log.info(_service);
      });

    ////
    // API
    ////

    return _service;

    ////
    // Functions
    ////

    function request(path, config) {
      return function (data) {
        var requestConfig = {
          method: config.method,
          url: basePath + '/' + path
        };
        switch (config.method) {
          case 'GET':
            requestConfig.params = data;
            break;
          case 'POST':
            requestConfig.data = data;
            break;
        }
        return $http(requestConfig)
          .then(function (response) {
            return response.data;
          })
          .catch(function (response) {
            $log.error('Error in request', response);
          });
      };
    }
  }
})();
