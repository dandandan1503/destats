/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('destats')
    .constant('moment', moment);

})();
